﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axede.Xynthesis.Log;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Axede.Xynthesis.IpcProcess;
using System.Text.RegularExpressions;

namespace GeneraIPCreporte
{
    public partial class FrmInicial : Form
    {
        private bool m_timerTaskSuccess;
        private int nroIntentos = 0;
        private LogError lg = new LogError();
        public DateTime fechaEje;
        string periodicidad = System.Configuration.ConfigurationSettings.AppSettings["Periodicidad"].ToString();
        string horaTrabajo = DateTime.Now.ToString("yyyy/MM/dd ") + System.Configuration.ConfigurationSettings.AppSettings["horaTrabajo"].ToString();
        string horas = System.Configuration.ConfigurationSettings.AppSettings["horaTrabajo"].ToString();
        string rutaRobot = System.Configuration.ConfigurationSettings.AppSettings["rutaRobot"].ToString();
        string progRobot = System.Configuration.ConfigurationSettings.AppSettings["progRobot"].ToString();
        string rutaDescargas = System.Configuration.ConfigurationSettings.AppSettings["rutaDescargas"].ToString();
        string nombreDescarga = System.Configuration.ConfigurationSettings.AppSettings["nombreDescarga"].ToString();
        string nroIntentos_ = System.Configuration.ConfigurationSettings.AppSettings["nroIntentos_"].ToString();
        string ActivaCargueBlueWave = System.Configuration.ConfigurationSettings.AppSettings["ActivaCargueBlueWave"].ToString();
        
        ClsBlueWave bwave = new ClsBlueWave();
        public FrmInicial()
        {
            InitializeComponent();
        }

        private void FrmInicial_Load(object sender, EventArgs e)
        {
            try
            {
                eliminaDescargas();
                Thread.Sleep(5000);  // 5 seg
                iniciarParametros();
            }
            catch (Exception ex)
            {
                lg.EscribaLog("GeneraIPreporte", "Error en metodo FrmInicial_Load " + ex.Message, "Administrador");
            }
        }

        public void eliminaDescargas()
        {
            try
            {
                var txtFiles = Directory.EnumerateFiles(rutaDescargas, nombreDescarga);
                foreach (string currentFile in txtFiles)
                {
                    string fileName = currentFile.ToString();
                    File.Delete(fileName);
                }

            }
            catch (Exception ex)
            {
                lg.EscribaLog("GeneraIPreporte", "Error en metodo eliminaDescargas " + ex.Message, "Administrador");
                throw ex;
            }
        }

        private void iniciarParametros()
        {
            try
            {
                lg.EscribaLog("GeneraIPreporte", "OnStart iniciando servicio cargue Etrali", "Administrador");
                fechaEje = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd") + " " + horas);
                if (periodicidad == "D")   //intervalo dias
                    trmReloj.Interval = 5000;  //5 Segundos
                else
                    trmReloj.Interval = convertiraMiliSegundos(Convert.ToInt32(horas.Substring(0, 2)), Convert.ToInt32(horas.Substring(3, 2)), Convert.ToInt32(horas.Substring(6, 2)));    // intervalo HORAS
                trmReloj.Start(); // Start
                m_timerTaskSuccess = false;
            }
            catch (Exception ex)
            {
                lg.EscribaLog("GeneraIPreporte", "Error en metodo iniciarParametros " + ex.Message, "Administrador");
                throw ex;
            }

        }

        private void trmReloj_Tick(object sender, EventArgs e)
        {
            Boolean resOk = false;
            try
            {

                var txtFiles = Directory.EnumerateFiles(rutaDescargas, nombreDescarga);
                foreach (string currentFile in txtFiles)
                {
                    resOk = true;
                    string fileName = currentFile.Substring(rutaDescargas.Length + 1);
                    trmReloj.Stop();
                    break;
                }
                if (!resOk)
                {
                    if (activaRobot())
                        System.Windows.Forms.Application.Exit();

                   
                    if (nroIntentos > Convert.ToInt32(nroIntentos_))
                    {
                        lg.EscribaLog("GeneraIPreporte", "trmReloj_Tick: el numero de intentos fue superior o igual a "+ nroIntentos_ + " veces. ", "Administrador");
                        trmReloj.Stop();
                        System.Windows.Forms.Application.Exit();
                    }

                }
                else
                {
                    System.Windows.Forms.Application.Exit();
                }

            }
            catch (Exception ex)
            {
                lg.EscribaLog("GeneraIPreporte", "Error en metodo trmReloj_Tick " + ex.Message, "Administrador");

                throw ex;

            }

        }

        public Boolean activaRobot()
        {
            try
            {
                DateTime fecIni = System.DateTime.Now;
                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = false;
                    startInfo.WorkingDirectory = rutaRobot;
                    startInfo.FileName = rutaRobot+progRobot;
                    startInfo.Arguments = "";
                    Process.Start(startInfo);

                }
                catch (Exception ex)
                {
                    lg.EscribaLog("GeneraIPreporte", "Error en el metodo activaRobot, El programa(exe) de generacion no se encontro en ruta: "+ rutaRobot + progRobot , "Administrador");
                    System.Windows.Forms.Application.Exit();
                    return true;

                }
                Thread.Sleep(150000);//   2.5 min.
                //iniciarParametros();
                nroIntentos++;
                string file = rutaDescargas + nombreDescarga.Replace("*", "");
                if (File.Exists(file))
                {
                    FileInfo oFileInfo = new FileInfo(file);
                    DateTime fecCreacion = oFileInfo.CreationTime;
                    if (fecCreacion > fecIni)
                    {
                        lg.EscribaLog("GeneraIPreporte", "activaRobot se ha generado con exito el archivo " + file + " con fecha " + fecCreacion.ToString(), "Administrador");
                        System.Windows.Forms.Application.Exit();
                        return true;
                    }
                    else
                    {
                        lg.EscribaLog("GeneraIPreporte", "activaRobot no se genero el archivo " + file +" fecha de intento "+fecIni.ToString(), "Administrador");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                lg.EscribaLog("GeneraIPreporte", "Error en el metodo activaRobot " + ex.Message, "Administrador");
                System.Windows.Forms.Application.Exit();
                return false;
                
            }
        }


        protected int convertiraMiliSegundos(int h, int m, int s)
        {
            return (h * 3600 + m * 60 + s) * 1000;
        }




    }
}
