﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Forms;
using Axede.Xynthesis.Log;
using System.Xml.Linq;

namespace GeneraIPCreporte
{
    //public class nodo
    //{
    //    public string lastModified { get; set; }
    //    public string id { get; set; }
    //    public string cLIName { get; set; }
    //    public string cLINumber { get; set; }
    //    public string buttonNumber { get; set; }
    //    public string callType { get; set; }
    //    public string callUsage { get; set; }
    //    public string destination { get; set; }
    //    public string deviceChannel { get; set; }
    //    public string deviceChannelType { get; set; }
    //    public string deviceIdId { get; set; }
    //    public string displayInCallHistory { get; set; }
    //    public string duration { get; set; }
    //    public string e164Destination { get; set; }
    //    public string eventType { get; set; }
    //    public string parentUserCDIId { get; set; }
    //    public string personalPointOfContactId { get; set; }
    //    public string pointOfContactId { get; set; }
    //    public string priority { get; set; }
    //    public string reasonForDisconnect { get; set; }
    //    public string resourceAORId { get; set; }
    //    public string rolloverAppearance { get; set; }
    //    public string routedDestination { get; set; }
    //    public string startTime { get; set; }
    //    public string trunkBChannel { get; set; }
    //    public string trunkId { get; set; }
    //    public string userId { get; set; }
    //}
    public class ClsBlueWave
    {
        //List<nodo> nod;
        //string blueWaveUrlsession = System.Configuration.ConfigurationSettings.AppSettings["blueWaveUrlsession"].ToString();
        //string blueWaveUrlHistorico = System.Configuration.ConfigurationSettings.AppSettings["blueWaveUrlHistorico"].ToString();
        
        //LogError lg = new LogError();
        //public ClsBlueWave()
        //{
            
        //}


        //public string iniciaSession()
        //{
        //    string AuthenticationToken = "";
        //    String fullFilePath = Application.StartupPath+"\\"+  @"XMLsession.xml";
        //    String uri = @blueWaveUrlsession;

        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);

        //    request.KeepAlive = false;
        //    request.Headers.Add("Authorization", "Basic " + "YXhlZGUxOmF4ZTExMDY=");
        //    request.ProtocolVersion = HttpVersion.Version10;
        //    request.ContentType = "application/xml";
        //    request.Method = "POST";

        //    StringBuilder sb = new StringBuilder();
        //    using (StreamReader sr = new StreamReader(fullFilePath))
        //    {
        //        String line;
        //        while ((line = sr.ReadLine()) != null)
        //        {
        //            sb.AppendLine(line);
        //        }
        //        byte[] postBytes = Encoding.UTF8.GetBytes(sb.ToString());
        //        request.ContentLength = postBytes.Length;

        //        try
        //        {
        //            Stream requestStream = request.GetRequestStream();

        //            requestStream.Write(postBytes, 0, postBytes.Length);
        //            requestStream.Close();
        //            string result = string.Empty;
        //            using (var response = (HttpWebResponse)request.GetResponse())
        //            {
        //                Console.WriteLine(response.ToString());

        //                using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
        //                {
        //                    result = httpWebStreamReader.ReadToEnd();
        //                }
        //            }
        //            XNamespace aw = "http://www.ipc.com/bw";
        //           // XElement root = new XElement(aw + "AuthenticationToken", string.Empty);

        //            XDocument doc = XDocument.Parse(result);
        //            foreach (XElement element in doc.Descendants(aw+"AuthenticationToken"))
        //            {
        //                AuthenticationToken = element.Value;
        //            }
        //            //Console.WriteLine(result);

        //            //Console.ReadLine();}
        //            return AuthenticationToken;
        //        }
        //        catch (Exception ex)
        //        {

        //            lg.EscribaLog("GeneraIPreporte", "Error en metodo iniciaSession:  " + ex.Message, "Administrador");
        //            request.Abort();
        //            return "";
        //        }
        //    }
        //}

        //public List<nodo> obtenerHistorico(string IPCAuthToken)
        //{

        //    String fullFilePath = Application.StartupPath + "\\" + @"XMLsession.xml";
        //    String uri = @blueWaveUrlHistorico+"?FilterType=datepage&starttime=3703017600&timezone=EST&timeformat=absolute";
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            
        //    try
        //    {
        //        request.KeepAlive = false;
        //        request.Headers.Add("X-IPCAuthToken", IPCAuthToken);
        //        request.ProtocolVersion = HttpVersion.Version10;
        //        request.ContentType = "application/xml";
        //        request.Method = "GET";
        //        string result = string.Empty;
        //        using (var response = (HttpWebResponse)request.GetResponse())
        //        {
        //            Console.WriteLine(response.ToString());

        //            using (StreamReader httpWebStreamReader = new StreamReader(response.GetResponseStream()))
        //            {
        //                result = httpWebStreamReader.ReadToEnd();
        //            }
        //        }
        //        XNamespace aw = "http://www.ipc.com/bw";
        //        // XElement root = new XElement(aw + "AuthenticationToken", string.Empty);

        //        XDocument doc = XDocument.Parse(result);
               
        //        foreach (XElement element in doc.Descendants(aw + "CommunicationHistory"))
        //        {
        //            nodo n = new nodo();
        //            n.id = element.Element(aw + "id").Value;
        //            n.lastModified= element.Element(aw + "lastModified").Value;
        //            n.cLIName = element.Element(aw + "cLIName").Value;
        //            n.cLINumber = element.Element(aw + "cLINumber").Value;
        //            n.buttonNumber = element.Element(aw + "buttonNumber").Value;
        //            n.callType = element.Element(aw + "callType").Value;
        //            n.callUsage = element.Element(aw + "callUsage").Value;
        //            n.destination = element.Element(aw + "destination").Value;
        //            n.deviceChannel = element.Element(aw + "deviceChannel").Value;
        //            n.deviceChannelType = element.Element(aw + "deviceChannelType").Value;
        //            n.deviceIdId = element.Element(aw + "deviceIdId").Value;
        //            n.displayInCallHistory = element.Element(aw + "displayInCallHistory").Value;
        //            n.duration = element.Element(aw + "duration").Value;
        //            n.e164Destination = element.Element(aw + "e164Destination").Value;
        //            n.eventType = element.Element(aw + "eventType").Value;
        //            n.parentUserCDIId = element.Element(aw + "parentUserCDIId").Value;
        //            n.personalPointOfContactId = element.Element(aw + "personalPointOfContactId").Value;
        //            n.pointOfContactId = element.Element(aw + "pointOfContactId").Value;
        //            n.priority = element.Element(aw + "priority").Value;
        //            n.reasonForDisconnect = element.Element(aw + "reasonForDisconnect").Value;
        //            n.resourceAORId = element.Element(aw + "resourceAORId").Value;
        //            n.rolloverAppearance = element.Element(aw + "rolloverAppearance").Value;
        //            n.routedDestination = element.Element(aw + "routedDestination").Value;
        //            n.startTime = element.Element(aw + "startTime").Value;
        //            n.trunkBChannel = element.Element(aw + "trunkBChannel").Value;
        //            n.trunkId = element.Element(aw + "trunkId").Value;
        //            n.userId = element.Element(aw + "userId").Value;
        //            nod.Add(n);                        
        //        }
        //        return nod;

        //    }
        //    catch (Exception ex)
        //    {

        //        lg.EscribaLog("GeneraIPreporte", "Error en metodo obtenerHistorico:  " + ex.Message, "Administrador");
        //        return null;
        //    }
        //}



    }
}
